#!/bin/bash

# create a self-signed temporary cert for testing
openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '//CN=localhost'
openssl rsa -in key.pem -out key.pem
