extern crate env_logger;
extern crate log;
use std::io::Write;

mod config;
mod scrum_poker;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=debug");
    std::env::set_var("RUST_BACKTRACE", "1");
    let mut builder = env_logger::Builder::from_default_env();
    builder.format(|buf, record| {
        let log_data: String = record.args().to_string();
        if !log_data.contains("GET /data") {
            writeln!(buf, "{} - {}", record.level(), log_data)
        } else {
            Ok(())
        }
    });
    builder.init();
    let mut config_path = std::env::current_exe().unwrap();
    config_path.pop();
    config_path.push("scrum_poker_config.json");
    let config = config::Config::from_path(String::from(config_path.to_str().unwrap()));
    scrum_poker::scrum_poker_start_server(config).await
}
