use crate::config;
use actix_files::NamedFile;
use actix_web::middleware::Logger;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::sync::Mutex;

#[derive(Serialize)]
struct RegisterAcknowledge {
    acknowledge: bool,
    unique_id: Option<u64>,
}

#[derive(Serialize, Deserialize, Copy, Clone, PartialEq)]
enum UserVote {
    None,
    QuestionMark,
    Vote(u8),
}

#[derive(Serialize)]
struct ScrumUser {
    name: String,
    unique_id: u64,
    vote: UserVote,
}

#[derive(Serialize)]
struct DataResponse<'a> {
    users: &'a Vec<ScrumUser>,
    is_user_alive: bool,
    board_name: &'a str,
    voting: VotingState,
    init_voting_time: u32,
    voting_time: u32,
    server_address: String,
    jira_summary: String,
    jira_description: String,
}

#[derive(Serialize, Copy, Clone, PartialEq)]
enum VotingState {
    End,
    InProgress,
}

struct ScrumBoard {
    #[allow(dead_code)]
    name: String,
    voting: VotingState,
    users: Vec<ScrumUser>,
    init_voting_time: u32,
    voting_time: u32,
    jira_summary: String,
    jira_description: String,
    // Todo:
    // user_keep_alive: Mutex<HashMap<u64, i64>>,
}

struct ScrumPokerState {
    web_resource: std::path::PathBuf,
    user_index: Mutex<u64>,
    config: config::Config,
    boards: Mutex<HashMap<String, ScrumBoard>>,
}

impl ScrumPokerState {
    fn next_index(&self) -> u64 {
        let mut index = self.user_index.lock().unwrap();
        *index = *index + 1;
        return *index;
    }
}

#[derive(Deserialize)]
struct RegisterForm {
    name: String,
}

static DEFAULT_BOARD_REQ: &'static str = "Test";

impl ScrumPokerState {
    fn default_new(config: config::Config) -> ScrumPokerState {
        let mut path = std::env::current_exe().expect(concat!(
            "Project assumption is that current_exe will never fail. ",
            "Do not use links or other not common filesystem configuration."
        ));
        if path.is_file() {
            path.pop();
        }
        assert!(path.is_dir());
        path.push("web");
        let mut board_map: HashMap<String, ScrumBoard> = HashMap::new();
        // For now there is only one default board for fota team.
        // Todo: Add possibility for multiboard scrum.
        let board_name = config.boards[0].clone();
        board_map.insert(
            board_name.clone(),
            ScrumBoard {
                name: board_name,
                users: Vec::new(),
                voting: VotingState::End,
                init_voting_time: 60,
                voting_time: 0,
                jira_summary: String::new(),
                jira_description: String::new(),
            },
        );
        ScrumPokerState {
            web_resource: path,
            user_index: Mutex::new(0),
            boards: Mutex::new(board_map),
            config: config,
        }
    }
}

async fn default_404(data: web::Data<ScrumPokerState>) -> impl Responder {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("not_found_404.html");
    let body = fs::read_to_string(path).unwrap_or(String::from("Empty!"));
    return HttpResponse::NotFound().body(body);
}

async fn index(data: web::Data<ScrumPokerState>) -> actix_web::Result<actix_files::NamedFile> {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("index.html");
    Ok(NamedFile::open(path)?)
}

#[get("/favicon.ico")]
async fn favicon(data: web::Data<ScrumPokerState>) -> actix_web::Result<actix_files::NamedFile> {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("favicon.ico");
    Ok(actix_files::NamedFile::open(path)?)
}

#[get("/background.png")]
async fn background(data: web::Data<ScrumPokerState>) -> actix_web::Result<actix_files::NamedFile> {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("background.png");
    Ok(actix_files::NamedFile::open(path)?)
}

#[get("/style.css")]
async fn style_css(data: web::Data<ScrumPokerState>) -> actix_web::Result<actix_files::NamedFile> {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("style.css");
    Ok(actix_files::NamedFile::open(path)?)
}

#[get("/scrum_poker.js")]
async fn scrum_poker_js(
    data: web::Data<ScrumPokerState>,
) -> actix_web::Result<actix_files::NamedFile> {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("scrum_poker.js");
    Ok(actix_files::NamedFile::open(path)?)
}

#[get("/board")]
async fn board_html(data: web::Data<ScrumPokerState>) -> actix_web::Result<actix_files::NamedFile> {
    let mut path: std::path::PathBuf = data.web_resource.clone();
    path.push("board.html");
    Ok(actix_files::NamedFile::open(path)?)
}

#[get("/register")]
async fn register(
    form: web::Query<RegisterForm>,
    data: web::Data<ScrumPokerState>,
) -> HttpResponse {
    let mut boards = data.boards.lock().unwrap();
    let board_req = String::from(DEFAULT_BOARD_REQ);
    if let Some(board) = boards.get_mut(&board_req) {
        let users = &mut board.users;
        let unique_id = data.next_index();
        users.push(ScrumUser {
            name: form.name.clone(),
            unique_id: unique_id,
            vote: UserVote::None,
        });
        return HttpResponse::Ok().json(RegisterAcknowledge {
            acknowledge: true,
            unique_id: Some(unique_id),
        });
    } else {
        return HttpResponse::Ok().json(RegisterAcknowledge {
            acknowledge: false,
            unique_id: None,
        });
    }
}

#[derive(Deserialize)]
struct DataRequest {
    unique_id: Option<u64>,
}

#[get("/data")]
async fn fetch_data(
    query: web::Query<DataRequest>,
    data: web::Data<ScrumPokerState>,
) -> HttpResponse {
    let mut boards = data.boards.lock().unwrap();
    let board_req = String::from(DEFAULT_BOARD_REQ);
    if let Some(board) = boards.get_mut(&board_req) {
        let users = &mut board.users;
        let mut is_user_alive = false;
        if let Some(unique_id) = query.unique_id {
            if users
                .iter()
                .find(|user| user.unique_id == unique_id)
                .is_some()
            {
                is_user_alive = true;
            }
        }
        return HttpResponse::Ok().json(DataResponse {
            users: &*users,
            is_user_alive: is_user_alive,
            board_name: &board.name,
            voting: board.voting,
            init_voting_time: board.init_voting_time,
            voting_time: board.voting_time,
            server_address: data.config.jira_server.clone(),
            jira_summary: board.jira_summary.clone(),
            jira_description: board.jira_description.clone(),
        });
    } else {
        return HttpResponse::Ok().json(DataResponse {
            users: &vec![],
            is_user_alive: false,
            board_name: "ERROR!",
            voting: VotingState::End,
            init_voting_time: 60,
            voting_time: 0,
            server_address: String::from(""),
            jira_summary: String::new(),
            jira_description: String::new(),
        });
    }
}

#[derive(Deserialize)]
struct DeleteRequest {
    unique_id: u64,
}

#[get("/delete")]
async fn delete_user(
    query: web::Query<DeleteRequest>,
    data: web::Data<ScrumPokerState>,
) -> HttpResponse {
    let mut boards = data.boards.lock().unwrap();
    let board_req = String::from(DEFAULT_BOARD_REQ);
    if let Some(board) = boards.get_mut(&board_req) {
        let users = &mut board.users;
        users.retain(|x| x.unique_id != query.unique_id);
    }
    return HttpResponse::Ok().body(actix_web::body::Body::Empty);
}

#[derive(Deserialize)]
struct StartStopVotingRequest {
    init_voting_time: u32,
}

#[get("/start_stop")]
async fn start_stop_voting(
    query: web::Query<StartStopVotingRequest>,
    data: web::Data<ScrumPokerState>,
) -> HttpResponse {
    let mut boards = data.boards.lock().unwrap();
    let board_req = String::from(DEFAULT_BOARD_REQ);
    if let Some(board) = boards.get_mut(&board_req) {
        match &board.voting {
            VotingState::End => {
                board.init_voting_time = query.init_voting_time;
                board.voting_time = board.init_voting_time;
                for user in &mut board.users {
                    user.vote = UserVote::None;
                }
                board.voting = VotingState::InProgress;
            }
            VotingState::InProgress => {
                board.voting = VotingState::End;
                board.voting_time = 0;
            }
        }
    }
    return HttpResponse::Ok().body(actix_web::body::Body::Empty);
}

#[derive(Deserialize)]
struct VotingRequest {
    vote: String,
    unique_id: u64,
}

#[get("/voting")]
async fn user_voting(
    query: web::Query<VotingRequest>,
    data: web::Data<ScrumPokerState>,
) -> HttpResponse {
    let mut boards = data.boards.lock().unwrap();
    let board_req = String::from(DEFAULT_BOARD_REQ);
    if let Some(board) = boards.get_mut(&board_req) {
        if board.voting == VotingState::End {
            return HttpResponse::Ok().body(actix_web::body::Body::Empty);
        }
        let res_user = board
            .users
            .iter_mut()
            .find(|user| user.unique_id == query.unique_id);
        if let Some(mut user) = res_user {
            match query.vote.as_str() {
                "None" => user.vote = UserVote::None,
                "?" => user.vote = UserVote::QuestionMark,
                "QuestionMark" => user.vote = UserVote::QuestionMark,
                other_str => {
                    user.vote = UserVote::Vote(other_str.parse::<u8>().unwrap());
                }
            }
            return HttpResponse::Ok().body(actix_web::body::Body::Empty);
        } else {
            return HttpResponse::BadRequest().body(actix_web::body::Body::Empty);
        }
    } else {
        panic!("Not any board is available!");
    }
}

#[derive(Deserialize)]
struct JiraRequest {
    user: String,
    password: String,
    story: String,
}

#[derive(Serialize)]
struct JiraResponse {
    summary: String,
    description: String,
}

#[get("/jira")]
async fn jira(query: web::Query<JiraRequest>, data: web::Data<ScrumPokerState>) -> HttpResponse {
    use actix_web::client::{ClientBuilder, Connector};
    let timeout = 60;
    let connector = Connector::new()
        .timeout(std::time::Duration::from_secs(timeout))
        .finish();
    let client = ClientBuilder::new()
        .connector(connector)
        .timeout(std::time::Duration::from_secs(timeout))
        .basic_auth(query.user.clone(), Some(&query.password))
        .finish();
    let mut url = String::from(data.config.jira_server.clone());
    url = url + "/rest/agile/1.0/issue/" + &query.story;
    let result_res = client
        .get(url)
        .header("User-Agent", "Actix-web")
        .header("Content-Type", "application/json")
        .send()
        .await;
    if let Ok(mut res) = result_res {
        let result_body = res.body().await;
        if let Ok(body) = result_body {
            if let Ok(json_str) = std::str::from_utf8(&body) {
                if let Ok(body_json) = serde_json::from_str::<serde_json::Value>(json_str) {
                    let mut boards = data.boards.lock().unwrap();
                    let board_req = String::from(DEFAULT_BOARD_REQ);
                    let mut summary = String::from("Do not exist");
                    let mut description = String::from("Do not exist");
                    if let Some(summary_str) = body_json["fields"]["summary"].as_str() {
                        summary = String::from(summary_str)
                    }
                    if let Some(description_str) = body_json["fields"]["description"].as_str() {
                        description = String::from(description_str)
                    }
                    if let Some(mut board) = boards.get_mut(&board_req) {
                        println!("Success get jira");
                        board.jira_summary = summary;
                        board.jira_description = description;
                    }
                } else {
                    println!("Error request body str to json failed");
                }
            } else {
                println!("Error request body utf8 to str failed");
            }
        } else {
            println!("Error request body");
        }
    } else {
        println!("Error request");
    }
    return HttpResponse::Ok().json(JiraResponse {
        summary: String::from("Ok"),
        description: String::from("Ok"),
    });
}

async fn scrum_poker_periodic_task(state: actix_web::web::Data<ScrumPokerState>) {
    // Loop every one seconds
    loop {
        actix_web::rt::time::delay_for(std::time::Duration::from_millis(1000)).await;
        {
            let mut boards = state.boards.lock().unwrap();
            let board_req = String::from(DEFAULT_BOARD_REQ);
            // Todo: add loop through all boards
            if let Some(board) = boards.get_mut(&board_req) {
                if board.voting_time < 1 {
                    board.voting_time = 0;
                    match &board.voting {
                        VotingState::InProgress => {
                            board.voting = VotingState::End;
                        }
                        _ => {}
                    }
                } else {
                    board.voting_time -= 1;
                    scrum_poker_check_state(board);
                }
            }
        }
    }
}

fn scrum_poker_check_state(board: &mut ScrumBoard) {
    let all_user_voted = board.users.iter().all(|user| user.vote != UserVote::None);
    if all_user_voted {
        board.voting = VotingState::End;
        board.voting_time = 0;
    }
}

fn load_ssl() -> rustls::ServerConfig {
    use rustls::internal::pemfile::{certs, rsa_private_keys};
    use rustls::{NoClientAuth, ServerConfig};
    use std::io::BufReader;
    const CERT: &'static [u8] = include_bytes!("../tls/cert.pem");
    const KEY: &'static [u8] = include_bytes!("../tls/key.pem");
    let mut cert = BufReader::new(CERT);
    let mut key = BufReader::new(KEY);
    let mut config = ServerConfig::new(NoClientAuth::new());
    let cert_chain = certs(&mut cert).unwrap();
    let mut keys = rsa_private_keys(&mut key).unwrap();
    config.set_single_cert(cert_chain, keys.remove(0)).unwrap();
    config
}

pub async fn scrum_poker_start_server(config: crate::config::Config) -> std::io::Result<()> {
    let server_address = config.socket_address.clone();
    let shared_state = actix_web::web::Data::new(ScrumPokerState::default_new(config));
    actix_web::rt::spawn(scrum_poker_periodic_task(shared_state.clone()));
    let config = load_ssl();
    HttpServer::new(move || {
        let logger = Logger::default();
        App::new()
            .wrap(logger)
            .app_data(shared_state.clone())
            .default_service(web::route().to(default_404))
            .route("/", web::get().to(index))
            .route("/index.html", web::get().to(index))
            .service(favicon)
            .service(background)
            .service(style_css)
            .service(scrum_poker_js)
            .service(board_html)
            .service(register)
            .service(fetch_data)
            .service(delete_user)
            .service(start_stop_voting)
            .service(user_voting)
            .service(jira)
    })
    .bind_rustls(server_address, config)?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;

    #[actix_rt::test]
    async fn test_index_do_not_panic() {
        let named_file = index(actix_web::web::Data::new(ScrumPokerState::default_new(
            config::Config {
                jira_server: String::from("Board1"),
                boards: vec![String::from("Test1")],
                socket_address: String::from(""),
            },
        )))
        .await
        .unwrap();
        let path = named_file.path().to_str().unwrap();
        assert!(path.ends_with("web\\index.html"), "Path is {}", path);
    }

    #[actix_rt::test]
    async fn test_default_404_do_not_panic() {
        default_404(actix_web::web::Data::new(ScrumPokerState::default_new(
            config::Config {
                jira_server: String::from("Board1"),
                boards: vec![String::from("Test1")],
                socket_address: String::from(""),
            },
        )))
        .await;
    }
}
