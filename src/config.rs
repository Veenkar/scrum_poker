use std::fs::File;

#[derive(serde::Deserialize)]
pub struct Config {
    pub jira_server: String,
    pub boards: Vec<String>,
    pub socket_address: String
}

trait PlatformDependency {
    fn file_open(&self, path: String) -> std::io::Result<Box<dyn std::io::Read>>;
    fn serde_json_from_reader(
        &self,
        reader: Box<dyn std::io::Read>,
    ) -> std::result::Result<Config, serde_json::Error>;
}

struct NativePlatform;
impl PlatformDependency for NativePlatform {
    fn file_open(&self, path: String) -> std::io::Result<Box<dyn std::io::Read>> {
        Ok(Box::new(File::open(path)?))
    }
    fn serde_json_from_reader(
        &self,
        reader: Box<dyn std::io::Read>,
    ) -> std::result::Result<Config, serde_json::Error> {
        serde_json::from_reader(reader)
    }
}

impl Config {
    pub fn from_path(path: String) -> Config {
        Config::from_path_platform(path, NativePlatform)
    }

    fn from_path_platform(path: String, platform: impl PlatformDependency) -> Config {
        let reader = platform
            .file_open(path)
            .expect("Run scrum_poker with json config file.");
        platform
            .serde_json_from_reader(reader)
            .expect("Invalid json provided for scrum_poker")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    struct MockPlatform;
    struct MockRead;
    impl std::io::Read for MockRead {
        fn read(&mut self, _buf: &mut [u8]) -> Result<usize, std::io::Error> {
            Ok(0)
        }
    }
    impl PlatformDependency for MockPlatform {
        fn file_open(&self, _path: String) -> std::io::Result<Box<dyn std::io::Read>> {
            Ok(Box::new(MockRead))
        }
        fn serde_json_from_reader(
            &self,
            _reader: Box<dyn std::io::Read>,
        ) -> std::result::Result<Config, serde_json::Error> {
            Ok(Config {
                jira_server: String::from("mock"),
                boards: vec![],
                socket_address: String::from("mock_socket_address"),
            })
        }
    }

    #[test]
    fn test_load_from_json_should_return_valid_config() {
        let example_json = serde_json::json!({
            "jira_server": "ExpectedServerAddress",
            "boards": ["Test1","Test2"],
            "socket_address": "ExpectedServerAddress"
        });
        let config: Config = serde_json::from_str(example_json.to_string().as_str()).unwrap();
        assert_eq!("ExpectedServerAddress", config.jira_server);
        assert_eq!(2, config.boards.len());
        assert_eq!("Test1", config.boards[0]);
        assert_eq!("Test2", config.boards[1]);
    }

    #[test]
    fn test_from_path_platform_should_return_config() {
        let config: Config =
            Config::from_path_platform(String::from("test_path.json"), MockPlatform);
        assert_eq!("mock", config.jira_server);
    }
}
