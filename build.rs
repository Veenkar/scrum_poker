use std::env;
extern crate fs_extra;
use fs_extra::dir::copy;
use std::fs;

fn main() {
    let mut options = fs_extra::dir::CopyOptions::new();
    options.overwrite = true;
    let mut resource_dir = std::path::PathBuf::from(env::var_os("OUT_DIR").unwrap());
    let upper_directories: std::path::PathBuf = ["..", "..", ".."].iter().collect();
    resource_dir.push(upper_directories);
    copy("web", &resource_dir, &options).unwrap();
    let mut config_json_target_path = resource_dir.clone();
    config_json_target_path.push("scrum_poker_config.json");
    fs::copy(
        ["example_config", "scrum_poker_config.json"]
            .iter()
            .collect::<std::path::PathBuf>(),
        config_json_target_path,
    )
    .unwrap();
    resource_dir.push("deps"); // for unittest
    copy("web", resource_dir, &options).unwrap();
}
