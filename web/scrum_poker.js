"use strict";
// user means the person that will take part in scrum voting or viewer

function check_local_storage_has_name() {
  return !(localStorage.getItem('name') === null);
}

function get_user_name(default_name = "") {
  let user_name = localStorage.getItem('name');
  if (null === user_name) {
    return default_name;
  }
  return user_name;
}

function init_user_data() {
  if (check_local_storage_has_name()) {
    let user_name = document.querySelectorAll('#user_name')
    console.assert(user_name.length == 1, `There can be only one id user_name not ${user_name.length}`);
    user_name = user_name[0];
    user_name.value = get_user_name("");
    console.log(`Loaded name ${user_name.value}`);
  }
  else {
    console.log("There is not local_storage with name.");
  }
}

function index_load_page_action() {
  init_user_data();
}

function lable_input_form__button__go_click() {
  console.log("lable_input_form__button__go_click");
  let user_name = document.querySelector("#user_name").value.trim();
  if (!user_name) {
    set_error("Your name is empty!");
  } else {
    set_error("");
    let query_data = {
      url: 'register',
      params: {
        'name': user_name,
      }
    }
    simple_get(function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          let rsp = JSON.parse(this.responseText);
          // console.log(rsp);
          if (rsp["acknowledge"]) {
            localStorage.setItem("name", user_name);
            localStorage.setItem("unique_id", rsp["unique_id"]);
            window.location.href = "board";
          }
          else {
            set_error(`Server reject name ${user_name}`);
          }
        }
        else {
          set_error("Error fetch register acknowledge from server");
        }
      }
    }, encodeQuery(query_data));
  }
}

function user_name_form_keydown_enter(event) {
  if (event.which == 13 || event.keyCode == 13 || event.key === "Enter") {
    lable_input_form__button__go_click();
  }
}

function button_reset_name_click() {
  localStorage.clear();
  window.location.href = "/";
}

function only_viewer_button_click() {
  localStorage.clear();
  window.location.href = "board";
}

function set_error(error) {
  if (error) {
    console.log(error);
    document.querySelector("#error").innerText = "Error: " + error;
  }
}

window.onload = (event) => {
  console.log(`Pathname ${window.location.pathname}`);
  if (window.location.pathname == "/board") {
    console.log("Board");
    load_data_from_server_in_loop();
    jira_story_changed();
  }
  else {
    index_load_page_action();
  }
};

function simple_get(work_func, url) {
  let http_request_timeout = 5000; // 5 seconds
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = work_func;
  xhttp.timeout = http_request_timeout;
  xhttp.open("GET", url, true);
  xhttp.send();
}

function load_data_from_server_in_loop() {
  let request_delay = 1000; // every second fetch update from server
  function load_delay() {
    setTimeout(function () {
      load_data_from_server_in_loop();
    }, request_delay);
  }
  let query_data = {
    url: 'data',
    params: {
      'unique_id': localStorage.getItem('unique_id'),
    }
  }
  simple_get(function () {
    if (this.readyState == 4) {
      try {
        if (this.status == 200) {
          update_data(JSON.parse(this.responseText));
        }
        else {
          set_error("Error fetch data from server");
        }
      }
      finally {
        load_delay();
      }
    }
  }, encodeQuery(query_data));
}

function update_user_list(model) {
  let user_list = document.querySelector("#user_list");
  if (Object.keys(model).length == 0 || model["users"].length == 0) {
    user_list.innerHTML = "Zero users";
  } else {
    let html_result = "";
    model["users"].forEach(function (value, index, array) {
      let is_you = value["unique_id"] == localStorage.getItem('unique_id');
      let is_you_text = is_you ? "You " : "";
      if (is_you || model["voting"] === "End") {
        if (value["vote"].hasOwnProperty("Vote")) {
          value["vote"] = value["vote"].Vote;
        } else if (value["vote"] === "QuestionMark") {
          value["vote"] = "?";
        }
      } else {
        value["vote"] = "X";
      }
      html_result = html_result + `
<div>
  <span>${is_you_text} ${value["name"]}</span>
  <span>
    Vote: <span>${value["vote"]}</span>
  </span>
  <button class="user_list__button_delete" onclick="user_list__button_delete_click(${value["unique_id"]})">
        Delete
  </button>
</div>
`;
    });
    if (html_result != user_list.innerHTML) {
      user_list.innerHTML = html_result;
    }
  }
}

function update_user(model) {
  let user = document.querySelector("#your_role");
  if (model["is_user_alive"]) {
    user.innerText = get_user_name("Only Viewer");
  }
  else {
    user.innerText = "Only Viewer";
    localStorage.clear();
  }
}

function update_voting_time(model) {
  if (update_voting_time.last_init_voting_time != model["init_voting_time"]) {
    document.querySelector("#init_voting_time").value = model["init_voting_time"];
  }
  update_voting_time.last_init_voting_time = model["init_voting_time"];
  document.querySelector("#voting_time").innerText = model["voting_time"];
}

function average(nums) {
  return nums.reduce((a, b) => (a + b)) / nums.length;
}

function median(values) {
  values.sort(function (a, b) { return a - b; });
  var half = Math.floor(values.length / 2);
  if (values.length % 2)
    return values[half];
  else
    return (values[half - 1] + values[half]) / 2.0;
}

function update_result(model) {
  let filtered_users = model["users"].filter(user => {
    let res = false;
    let is_not_none_question = user["vote"] !== "None" && user["vote"] !== "?" && user["vote"] !== "QuestionMark";
    if (is_not_none_question) {
      const is_nan = isNaN(parseInt(user["vote"]));
      if (is_nan) {
        console.log("Error vote is NaN");
      } else {
        res = true;
      }
    }
    return res;
  });
  let votes = filtered_users.map(user => parseInt(user["vote"]));
  if (votes.length == 0 || model["voting"] === "InProgress") {
    document.querySelector("#result_max").innerText = "None";
    document.querySelector("#result_min").innerText = "None";
    document.querySelector("#result_average").innerText = "None";
    document.querySelector("#result_med").innerText = "None";
  } else {
    document.querySelector("#result_max").innerText = Math.max.apply(Math, votes);
    document.querySelector("#result_min").innerText = Math.min.apply(Math, votes);
    document.querySelector("#result_average").innerText = average(votes);
    document.querySelector("#result_med").innerText = median(votes);
  }
}

function update_data(model) {
  let board_name_id = "#board_name";
  if (model["board_name"]) {
    document.querySelector(board_name_id).innerText = model["board_name"];
  } else {
    document.querySelector(board_name_id).innerText = "";
    set_error("Server response error (do not contains board_name)!");
  }

  document.querySelector("#voting_status").innerText = model["voting"];
  update_voting_time(model);

  update_user_list(model);
  update_user(model);
  update_result(model);
  update_jira_server(model);
  document.querySelector("#jira_summary").innerText = model["jira_summary"];
  document.querySelector("#jira_description").innerText = model["jira_description"];
}

function update_jira_server(model) {
  document.querySelector("#server_address").innerText = model["server_address"];
}

function encodeQuery(data) {
  let query = data.url
  if (data.params) {
    query += '?';
    for (let d in data.params)
      if (data.params[d] !== null && data.params[d] !== undefined) {
        query += encodeURIComponent(d) + '='
          + encodeURIComponent(data.params[d]) + '&';
      }
    return query.slice(0, -1)
  }
  return query
}

function user_list__button_delete_click(id) {
  let query_data = {
    url: 'delete',
    params: {
      'unique_id': id,
    }
  }
  simple_get(function () {
    if (this.readyState == 4) {
      if (this.status != 200) {
        set_error("Error delete");
      }
    }
  }, encodeQuery(query_data));
}

function start_stop_voting() {
  let query_data = {
    url: 'start_stop',
    params: {
      'init_voting_time': document.querySelector("#init_voting_time").value,
    }
  }
  simple_get(function () {
    if (this.readyState == 4) {
      if (this.status != 200) {
        set_error("Error fetch data from server");
      }
    }
  }, encodeQuery(query_data));
}

function user_vote(vote_str) {
  let query_data = {
    url: 'voting',
    params: {
      'vote': vote_str,
      'unique_id': localStorage.getItem('unique_id')
    }
  }
  simple_get(function () {
    if (this.readyState == 4) {
      if (this.status != 200) {
        set_error("Error user vote");
      }
    }
  }, encodeQuery(query_data));
}

function jira_story_changed() {
  document.querySelector("#ref_jira_story").innerText = document.querySelector("#jira_story").value;
}

function jira_load_button_click() {
  let query_data = {
    url: 'jira',
    params: {
      'user': document.querySelector("#jira_user").value,
      'password': document.querySelector("#jira_password").value,
      'story': document.querySelector("#jira_story").value,
    }
  }
  simple_get(function () {
    if (this.readyState == 4) {
      if (this.status != 200) {
        //set_error("Error jira load");
      } else {
        update_jira_story(JSON.parse(this.responseText));
      }
    }
  }, encodeQuery(query_data));
}

function update_jira_story(model) {
  document.querySelector("#jira_summary").innerText = model["summary"];
  document.querySelector("#jira_description").innerText = model["description"];
}